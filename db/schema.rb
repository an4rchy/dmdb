# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131102173941) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "movies", force: true do |t|
    t.string   "title"
    t.string   "language"
    t.date     "release_date"
    t.integer  "runtime"
    t.text     "summary"
    t.string   "alt_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "participations", force: true do |t|
    t.integer  "movie_id"
    t.integer  "role_id"
    t.integer  "person_id"
    t.string   "character"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "participations", ["movie_id"], name: "index_participations_on_movie_id", using: :btree
  add_index "participations", ["person_id"], name: "index_participations_on_person_id", using: :btree
  add_index "participations", ["role_id"], name: "index_participations_on_role_id", using: :btree

  create_table "people", force: true do |t|
    t.string   "name"
    t.string   "alt_name"
    t.date     "date_of_birth"
    t.text     "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
