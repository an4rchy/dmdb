class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.string :language
      t.date :release_date
      t.integer :runtime
      t.text :summary
      t.string :alt_title

      t.timestamps
    end
  end
end
