class CreateParticipations < ActiveRecord::Migration
  def change
    create_table :participations do |t|
      t.references :movie, index: true
      t.references :role, index: true
      t.references :person, index: true
      t.string :character

      t.timestamps
    end
  end
end
