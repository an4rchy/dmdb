class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :alt_name
      t.date :date_of_birth
      t.text :bio

      t.timestamps
    end
  end
end
