class Movie < ActiveRecord::Base
  validates :title, length: {maximum: 149}
end
