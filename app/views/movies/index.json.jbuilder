json.array!(@movies) do |movie|
  json.extract! movie, :title, :language
  json.url movie_url(movie, format: :json)
end
